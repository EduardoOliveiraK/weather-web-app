import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)
const http = Vue.http

http.options.root = process.env.VUE_APP_API_URL

const list = {
  weather: {method: 'GET', url: '?command=location&woeid={id}'}
}

export default Vue.resource('', {}, { ...list })
